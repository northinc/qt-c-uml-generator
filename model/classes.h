#ifndef CLASSES_H
#define CLASSES_H

#include <QObject>
#include <QColor>
#include <QImage>
#include <QVariant>
#include <QPoint>
#include <QPointF>
#include <QList>
#include <QHash>
#include <QSet>
#include <QRectF>

class ObjectList;
class Links;
class Model;
class ClassProp;
class ClassModel;

#include "../model_engine/objectlist.h"
#include "../model_engine/undoer.h"
#include "model.h"
#include "classprop.h"
#include "classmodel.h"
#include "links.h"

#endif // CLASSES_H
